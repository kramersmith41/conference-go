import requests
from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY


def get_image(city, state):
    headers = {
        "Authorization": PEXELS_API_KEY,
    }
    url = f"https://api.pexels.com/v1/search?query={city}+{state}"
    resp = requests.get(url, headers=headers)
    data = resp.json()
    return data["photos"][0]["src"]["original"]


def get_coordinates(city, state):
    url = f"http://api.openweathermap.org/geo/1.0/direct?q={city},{state},US&limit=5&appid={OPEN_WEATHER_API_KEY}"
    resp = requests.get(url)
    data = resp.json()
    lat = data[0]["lat"]
    lon = data[0]["lon"]
    return lat, lon


def get_weather_data(city, state):
    coords = get_coordinates(city, state)
    lat = coords[0]
    lon = coords[1]
    url = f"https://api.openweathermap.org/data/2.5/weather?lat={lat}&lon={lon}&appid={OPEN_WEATHER_API_KEY}&units=imperial"
    resp = requests.get(url)
    data = resp.json()
    return data["weather"][0]["description"], data["main"]["temp"]
